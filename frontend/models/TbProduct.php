<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_product".
 *
 * @property integer $product_id
 * @property string $product_name_th
 * @property integer $product_status
 * @property integer $product_promotion
 * @property string $image
 * @property string $product_name_en
 */
class TbProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_name_th', 'product_status', 'product_promotion', 'image'], 'required'],
            [['product_status', 'product_promotion'], 'integer'],
            [['product_name_th', 'image', 'product_name_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'product_name_th' => 'Product Name Th',
            'product_status' => 'Product Status',
            'product_promotion' => 'Product Promotion',
            'image' => 'Image',
            'product_name_en' => 'Product Name En',
        ];
    }
}
