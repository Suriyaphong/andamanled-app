<?php
namespace easycommerce\assets;

use yii\web\AssetBundle;

class EasyCommerceAsset extends AssetBundle
{
    public $sourcePath = '@easycommerce/assets';
    public $css = [
        'css/admin.css',
        'css/styles.css',
        'css/woocommerce-layout.css',
        'css/woocommerce-smallscreen.css',
        'font-awesome-4.7.0/css/font-awesome.min.css',
        'css/jquery.sidr.dark.min.css',
        'css/slick.min.css',
        'css/style.css',
        
    ];

    public $js = [
            'js/jquery-migrate.min.js',
            'js/function.js',
            'js/woocommerce.min.js',
            'js/jquery.blockUI.min.js',
            'js/js.cookie.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        
    ];
}