<?php 
use yii\helpers\Html;
?>
<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">    
		<div class="colophon-inner colophon-grid-2">
		    <div class="copyright">
		    	Copyright © 2017 Andaman PT. All Rights Reserved		    	
		    </div><!-- .copyright -->
			<div class="site-info">
		    	AndamanLED by <a target="_blank" rel="designer" href="https://wenthemes.com/">Andaman Web Developer</a>		    	
		    </div><!-- .site-info -->
		</div><!-- .colophon-inner -->
	</div><!-- .container -->
</footer>