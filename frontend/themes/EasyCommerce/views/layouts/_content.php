<?php 
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
?>

<div id="content" class="site-content">
	<?php echo $this->render('_slider.php');?>
	<div class="container">
		<div class="inner-wrapper">
			<?= $content ?>
		</div>
<!-- .inner-wrapper -->
	</div><!-- .container -->
</div>
