<?php 
use easycommerce\widgets\Menu;
?>
<div id="main-nav" class="clear-fix">
    <div class="container">
            <div class="wrap-menu-content">
                <nav class="navbar navbar-inverse">
                    <div class="container-fluid">
                        <ul class="nav navbar-nav navbar-left">
                            <li id="tab_A">
                                <a data-toggle="tab" href="#home5"><i class="fa fa-home"></i> HOME<br>หน้าหลัก</a>
                            </li>
                            <li id="tab_I">
                                <a href="#"><i class="fa fa-users"></i> ABOUT US <br>เกี่ยวกับเรา</a>
                            </li>
                            <li id="tab_C">
                                <a data-toggle="tab" href="#home5"><i class="fa fa-credit-card"></i> 
                                    QUEUE MAMAGEMENT SYSTEM <br>ระบบบริหารจัดการคิว</a>
                            </li>
                            <li id="tab_C">
                                <a data-toggle="tab" href="#home5"><i class="fa fa-desktop"></i> 
                                    ELECTRONICS DISPLAY BOARD <br>ป้ายแสดงผลอิเล็กทรอนิกส์</a>
                            </li>
                            <li id="tab_C">
                                <a data-toggle="tab" href="#home5"><i class="fa fa-car"></i> 
                                    PARKING MANAGEMENT SYSTEM <br>ระบบจัดการลานจอดรถยนต์</a>
                            </li>
                            <li id="tab_B">
                                <a data-toggle="tab" href="#home5"><i class="fa fa-cogs"></i> REFERENCE<br>ผลงานการติดตั้ง</a>
                            </li>
                            <li id="tab_J">
                                <a href="#"><i class="fa fa-phone-square"></i> CONTACT <br>ติดต่อเรา</a>
                            </li>
                            <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
                        </ul>
                        <!-- <ul class="nav navbar-nav navbar-right">
                          <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                        </ul> -->
                  </div>
                </nav>
                </div>                 
            </div><!-- .menu-content -->
    </div> <!-- .container -->
<?php
    $script = <<< JS
    $("#tab_A").click(function (e) {               
        window.location.replace("/site/index");
    });
    $("#tab_B").click(function (e) {               
        window.location.replace("/site/reference");
    });
     $("#tab_C").click(function (e) {               
        window.location.replace("/site/product");
    });
     $("#tab_D").click(function (e) {               
        window.location.replace("/site/promotion");
    });
     $("#tab_I").click(function (e) {               
        window.location.replace("/site/aboutus");
    });
     $("#tab_J").click(function (e) {               
        window.location.replace("/site/contact-us");
    });
JS;
$this->registerJs($script);
?>
</script><script src="/assets/845db602/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>