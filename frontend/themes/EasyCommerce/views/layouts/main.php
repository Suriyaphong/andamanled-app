<?php $this->beginContent('@easycommerce/views/layouts/_base.php'); ?>


<div id="page" class="hfeed site">
    <?= $this->render('_header.php',[]) ?>
    <?= $this->render('_menu.php');?>
    <?= $this->render('_content.php',['content'=>$content]) ?>
    <?= $this->render('_footer.php') ?>
</div>


<?php $this->endContent(); ?>