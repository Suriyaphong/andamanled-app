<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Menu;
?>
<header id="masthead" class="site-header" role="banner">
    <div class="container">
        <div class="inner-wrapper">              
            <div class="site-branding">
                <a href="#" class="custom-logo-link" rel="home" itemprop="url">
                    <img width="75" height="75" src="/images/logo.jpg" class="custom-logo" alt="Easy Commerce" itemprop="logo"></a>
                        <div id="site-identity">
                            <p class="site-title"><a href="index" rel="home">Andaman LED</a></p>
                            <p class="site-description" style="line-height:  0;">Andaman Pattana Ltd.</p>    
                            <!-- <div class="fb-share-button" data-href="<?= $urlweb ?>" data-layout="button_count" data-size="large" data-mobile-iframe="true">
                                <a class="fb-xfbml-parse-ignore" target="_blank">แชร์</a>
                            </div> -->
                        </div><!-- #site-identity -->
            </div><!-- .site-branding -->
        </div>
    </div>
</header>
