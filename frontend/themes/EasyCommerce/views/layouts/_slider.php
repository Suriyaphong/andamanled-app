<?php
use yii\bootstrap\Carousel; 

$text = "ห้างหุ้นส่วนจำกัด อันดมันพัฒนา เป็นผู้นำเข้าและจัดจำหน่ายสินค้าระบบรักษาความปลอดภัยแบบครบวงจร ระบบรักษาความปลอดภัยแบบครบวงจร ระบบรักษาความปลอดภัยแบบครบวงจร ระบบรักษาความปลอดภัยแบบครบวงจร ปลอดภัยแบบครบวงจร ระบบรักษาความปลอดภัยแบ ระบบรักษาความปลอดภัยแบบครบวงจร ตามมาตราฐานนานาชาติ (International Security Equipment and System for Premises and Properties) และเป็นศูนย์รวมอุปกรณ์ระบบกล้องวงจรปิด CCTV";
?>
<?php if(Yii::$app->controller->action->id == 'index') {?>
 	<div class="container">
			<div class="col-md-3">
		      	<div class="thumbnail" style="height:515px">
			        <a href="/images/1.jpg" target="_blank">
			          	<img src="/images/1.jpg" alt="Lights" style="width:90%;margin-top:15px;">
			        </a>
			          	<div class="caption" style="line-height:  1;">
			            	<p><?php echo $text; ?></p>
			          	</div>
		      	</div>
		    </div>
  			<div class="col-sm-9">
  				<div class="carousel-inner" style="height:515px">
  					<?php 
			        echo Carousel::widget([
					    'items' => [
					        	'<img src="/images/s_01.jpg"/>',
					        [
					        	'content' => '<img src="/images/s_02.jpg"/>',
					        ],
					        [
					            'content' => '<img src="/images/s_03.jpg"/>',
					        ],
					    ],
					    'controls' =>  [
					        '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					        <span class="sr-only">Previous</span>',
					        '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					        <span class="sr-only">Next</span>'
					    ],
					    'options' => [
					    	'class' => 'carousel slide'
					    ]
					]);?>
  			</div>
  		</div>
	</div>
<?php }?>