<?php 
namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Footer extends Widget
{
    public $items = [];

    const FULL_WIDTH = 12;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->renderItems();
    }

    protected function renderItems()
    {
        //$n = count($this->items);
        $content = '';
        $index = 1;
        $items = [];
        $groupItems = ArrayHelper::index($this->items, null, 'row');
        $n = count($groupItems);
        $rows = (int)(self::FULL_WIDTH / $n);

        $content .= Html::beginTag('div', ['class' => 'row']);
        for ($row = 1; $row <= $n; $row++) {
            if(isset($groupItems[$index])){
                $menus = ArrayHelper::index($groupItems[$index], null, 'title');
                
                $content .= Html::beginTag('div', ['class' => 'col-md-'.$rows.' col-sm-6 col-xs-12 footer-block']);
                foreach($menus as $key => $menu){
                    $content .= Html::tag('h2',$key,[]);
                    $content .= FooterMenu::widget([
                        'items' => $menu,
                        'submenuTemplate' => "\n{items}\n",
                        'encodeLabels' => false,
                    ]);
                    $content .= '<br>';
                }
                $content .= Html::endTag('div');
            }
            $index++;
        }
        $content .= Html::endTag('div');
        return $content;
    }
}
?>