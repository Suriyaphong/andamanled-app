<?php
use yii\widgets\Menu;
use yii\helpers\Html;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Nav;
$this->registerJs('$("#tab_C").addClass("active");');
$this->title = 'Andaman LED';
?>
<div class="container">
    <div class="col-sm-3">
        <h4><center><b>สินค้า / PRODUCT</b></center></h4>
            <?php echo Nav::widget([
            'items' => [
                [
                    'label' => 'Home',
                    'url' => ['site/index'],
                    'linkOptions' => [],
                ],
                [
                    'label' => 'Dropdown',
                    'items' => [
                         ['label' => 'Level 1 - Dropdown A', 'url' => '#'],
                         '<li class="divider"></li>',
                         '<li class="dropdown-header">Dropdown Header</li>',
                         ['label' => 'Level 1 - Dropdown B', 'url' => '#'],
                    ],
                ],
                [
                    'label' => 'Login',
                    'url' => ['site/login'],
                    'visible' => Yii::$app->user->isGuest
                ],
            ],
            'options' => ['class' =>'nav-pills'], // set this to nav-tab to get tab-styled navigation
        ]); ?>
    </div>

    <div class="col-sm-9">
        <h2><center><b><i class="fa fa-tags"></i> โปรโมชั่น / PROMOTION</b></center></h2>
        <div class="bs-example" data-example-id="simple-thumbnails"> 
            <div class="row"> 
                <div class="col-sm-3"> 
                    <a href="/images/logo.jpg" target="_blank" class="thumbnail">
                        <img alt="100%x180" data-src="/images/logo.jpg" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> 
                        <h3><center>ไม้กั้น</center></h3>
                    </a>
                </div>  
                <div class="col-sm-3"> 
                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                </div> 
                <div class="col-sm-3"> 
                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                </div> 
                <div class="col-sm-3">  
                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                </div> 
            </div> 
        </div>
    </div>


</div>

<br>
