<?php
use yii\bootstrap\Collapse;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\bootstrap\Carousel; 
use metronic\widgets\portlet\PortletBox;
$this->registerJs('$("#tab_A").addClass("active");');
$urlweb = "http://andamanled.com/";
$url = "https://www.facebook.com/%E0%B8%AD%E0%B8%B1%E0%B8%99%E0%B8%94%E0%B8%B2%E0%B8%A1%E0%B8%B1%E0%B8%99%E0%B8%9E%E0%B8%B1%E0%B8%92%E0%B8%99%E0%B8%B2-1296980640359541/?pnref=lhc";
$this->title = 'Andaman LED';
?>
<div class="container"> 
    <hr>
    <div class="col-sm-3">
        <div class="thumbnail">
            <h4><center><b><i class="fa fa-facebook-square" aria-hidden="true"></i> FACEBOOK PAGE</b></center></h4>
            <div class="fb-page" data-href="https://www.facebook.com/%E0%B8%AD%E0%B8%B1%E0%B8%99%E0%B8%94%E0%B8%B2%E0%B8%A1%E0%B8%B1%E0%B8%99%E0%B8%9E%E0%B8%B1%E0%B8%92%E0%B8%99%E0%B8%B2-1296980640359541/?pnref=lhc" data-tabs="timeline" data-width="300" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                <blockquote cite="https://www.facebook.com/%E0%B8%AD%E0%B8%B1%E0%B8%99%E0%B8%94%E0%B8%B2%E0%B8%A1%E0%B8%B1%E0%B8%99%E0%B8%9E%E0%B8%B1%E0%B8%92%E0%B8%99%E0%B8%B2-1296980640359541/?pnref=lhc" class="fb-xfbml-parse-ignore">
                    <a href="https://www.facebook.com/%E0%B8%AD%E0%B8%B1%E0%B8%99%E0%B8%94%E0%B8%B2%E0%B8%A1%E0%B8%B1%E0%B8%99%E0%B8%9E%E0%B8%B1%E0%B8%92%E0%B8%99%E0%B8%B2-1296980640359541/?pnref=lhc">อันดามันพัฒนา</a>
                </blockquote>
            </div>
        </div>
    </div>
    <div class="col-sm-9">  
        <div class="thumbnail">
            <h4><center><b><i class="fa fa-cogs" aria-hidden="true"></i> ผลงานการติดตั้ง / REFERENCE</b></center></h4>
            <?php 
            echo Carousel::widget([
                'items' => [
                    [
                        'content' => '<img src="/images/test2.jpg"/>',
                        'caption' => '<p>ติดตั้งระบบบัตรคิว และจอแสดงผลขนาดใหญ่โรงพยาบาลราชวิถี</p>',
                    ],
                        
                    [
                        'content' => '<img src="/images/test3.jpg"/>',
                        'caption' => '<p>ติดตั้งระบบบัตรคิว และจอแสดงผลขนาดใหญ่โรงพยาบาลราชวิถี</p>',
                    ],
                    [
                        'content' => '<img src="/images/test4.jpg"/>',
                        'caption' => '<p>ติดตั้งระบบบัตรคิว และจอแสดงผลขนาดใหญ่โรงพยาบาลราชวิถี</p>',
                    ],
                ],
                'controls' =>  [
                    '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>',
                    '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>'
                ],
                'options' => [
                    'class' => 'carousel slide'
                ]
            ]);?>
        </div>
    </div>
</div>
<div class="container"> 
    <div class="col-sm-12"> 
    <hr> 
    <h2><b><i class="fa fa-tags"></i> โปรโมชั่น / PROMOTION</b></h2>
        <div class="bs-example" data-example-id="simple-thumbnails"> 
            <div class="row"> 
                <div class="col-md-3 col-xs-6"> 
                    <a href="#" class="thumbnail"><img alt="100%x180" data-src="/images/logo.jpg" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                </div> 
                <div class="col-md-3 col-xs-6"> 
                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                </div> 
                <div class="col-md-3 col-xs-6"> 
                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                </div> 
                <div class="col-md-3 col-xs-6"> 
                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                </div> 
            </div> 
        </div>
    </div>
</div>
<br>
<?php
Modal::begin([
    'header' => '<h2>Line ID</h2>',
    'id' => 'modal_line'
]);

echo '<center>'.Html::img('/images/line-add.jpg',['width'=>'250','height'=>'250']).'<h4>LIND ID : @Andaman_Pattana</h4></center>';
Modal::end();
?>
<script type="text/javascript">
function modalshow(){
    $('#modal_line').modal('show');
}
</script>
