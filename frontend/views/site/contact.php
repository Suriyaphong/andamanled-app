<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Modal;


$this->registerJs('$("#tab_J").addClass("active");');
$this->title = 'Andaman LED';
?>
<div class="container">  
    
        <div class="col-sm-8" >
            <div class="thumbnail">
            <style>
            #map { height: 400px; width: 100%; }
            </style>
            <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css"
                integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ=="
                crossorigin=""/>
            <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"
                integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log=="
                crossorigin="">
            </script>


            <p></p>
            <div id="map"></div>

            <script type="text/javascript">
            var map = L.map('map').setView([13.9408916,100.6116939,'17.75z'],15);

            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);

            L.marker([13.9408916,100.6116939,'17.75z']).addTo(map)
                .bindPopup('ANDAMAN PATTANA LTD.')
                .openPopup();
            </script>
        </div>
    </div>
        <div class="col-sm-4" >
            <div class="thumbnail" style="height: 437px">
            <div class="c-section">
                <h3><center>ANDAMAN PATTANA LTD.</center></h3>
            </div>
            <div class="c-section" style="line-height:1">
                <div class="c-content-label uppercase bg-blue">Address</div>
                    <p>22/87 ซ.วิภาวดีรังสิต 35
                    <br>ถ.วิภาวดีรังสิต สีกัน ดอนเมือง
                    <br>กรุงเทพมหานคร 10210</p>
            </div>
            <div class="c-section" style="line-height:1">
                <div class="c-content-label uppercase bg-blue">Contacts</div>
                <p>
                    <strong><i class="fa fa-phone"></i></strong> 800 123 0000
                    <br>
                    <strong><i class="fa fa-fax"></i></strong> 800 123 8888</p>
            </div>
            <div class="c-section">
                <div class="c-content-label uppercase bg-blue">Social</div>
                <div class="form-group"  role="group">
                    <?= Html::a(Html::img('/images/line-icon.png',['width'=>'50','hight'=>'50']),false,['role'=>'modal-remote','onclick'=>'modalshow();'])?>
                    <?= Html::a(Html::img('/images/social-facebook-box-blue-icon.png',['width'=>'50','hight'=>'50']),'https://www.facebook.com/profile.php?id=100015042077067',['target'=>'_bank'])?>
                    <?= Html::img('/images/Mail-icon.png',['width'=>'50','height'=>'50']);?>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<?php
Modal::begin([
    'header' => '<h2>Line ID</h2>',
    'id' => 'modal_line'
]);

echo '<center>'.Html::img('/images/line-add.jpg',['width'=>'250','height'=>'250']).'<h4>LIND ID : @Andaman_Pattana</h4></center>';
Modal::end();
?>
<script type="text/javascript">
function modalshow(){
    $('#modal_line').modal('show');
}
</script>



