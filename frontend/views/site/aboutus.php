<?php
$header = "โปรแกรมเรียกบัตรคิว";
$detail = "โรงพยาบาลมะเร็ง อุดรธานี จังหวัดอุดรธานี";
$this->registerJs('$("#tab_I").addClass("active");');
$this->title = 'Andaman LED';
?>
<div class="container"> 
    <div class="col-md-12">
        <div class="col-md-9">
            <div class="thumbnail">
            <img src="/images/banner.jpg" alt="Lights" style="height: 423px;width:100%">
        </div>
        </div>
        <div class="col-sm-3">
            <div class="thumbnail"> 
                <img alt="100%x200" data-src="holder.js/100%x200" style="height: 250px; width: 100%; display: block;" src="/images/user.jpg" data-holder-rendered="true">
                <div class="caption"> 
                    <h3 style="text-align: center;">Thumbnail label</h3> 
                    <p style="line-height: 1">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
                     
                </div> 
            </div> 
        </div>
        
    </div>
    <div class="col-sm-12">
        <hr>
        <div class="container text-center">
            <h2>ทีมงาน Andaman Pattana</h2>
        </div>
        <div class="container">    
        <div class="bs-example" data-example-id="thumbnails-with-custom-content"> 
            <div class="row"> 
                <div class="col-sm-3">
                    <div class="thumbnail"> 
                        <img alt="100%x200" data-src="holder.js/100%x200" style="height: 250px; width: 100%; display: block;" src="/images/550753-200.png" data-holder-rendered="true">
                        <div class="caption"> 
                            <h3>Thumbnail label</h3> 
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
                             
                        </div> 
                    </div> 
                </div>
                <div class="col-sm-3">
                    <div class="thumbnail"> 
                        <img alt="100%x200" data-src="holder.js/100%x200" style="height: 250px; width: 100%; display: block;" src="/images/550753-200.png" data-holder-rendered="true">
                        <div class="caption"> 
                            <h3>Thumbnail label</h3> 
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
                            
                        </div> 
                    </div> 
                </div>
                <div class="col-sm-3">
                    <div class="thumbnail"> 
                        <img alt="100%x200" data-src="holder.js/100%x200" style="height: 250px; width: 100%; display: block;" src="/images/550753-200.png" data-holder-rendered="true">
                        <div class="caption"> 
                            <h3>Thumbnail label</h3> 
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
                            
                        </div> 
                    </div> 
                </div>
                <div class="col-sm-3"> 
                    <div class="thumbnail"> 
                        <img alt="100%x200" data-src="holder.js/100%x200" style="height: 250px; width: 100%; display: block;" src="/images/550753-200.png" data-holder-rendered="true">
                        <div class="caption"> 
                            <h3>Thumbnail label</h3> 
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
                             
                        </div> 
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <hr>
        <div class="container text-center">
            <h2>ผลงานที่ผ่านมาของ</h2>
        </div>
        <div class="container">    
            <div class="bs-example" data-example-id="thumbnails-with-custom-content"> 
            <div class="row"> 
                <div class="col-sm-12">
                    <div class="thumbnail"> 
                        <div class="bs-example" data-example-id="simple-thumbnails"> 
                            <div class="row"> 
                                <div class="col-md-3 col-xs-6"> 
                                    <a href="#" class="thumbnail"><img alt="100%x180" data-src="/images/logo.jpg" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                                </div> 
                                <div class="col-md-3 col-xs-6"> 
                                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                                </div> 
                                <div class="col-md-3 col-xs-6"> 
                                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                                </div> 
                                <div class="col-md-3 col-xs-6"> 
                                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                                </div> 
                            </div> 
                        </div>
                        <div class="caption"> 
                            <h3>Thumbnail label</h3> 
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
                            <p><a href="#" class="btn btn-primary" role="button">Button</a> 
                                <a href="#" class="btn btn-default" role="button">Button</a>
                            </p> 
                        </div> 
                    </div> 
                </div>
                <div class="col-sm-12">
                    <div class="thumbnail"> 
                        <div class="bs-example" data-example-id="simple-thumbnails"> 
                            <div class="row"> 
                                <div class="col-md-3 col-xs-6"> 
                                    <a href="#" class="thumbnail"><img alt="100%x180" data-src="/images/logo.jpg" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                                </div> 
                                <div class="col-md-3 col-xs-6"> 
                                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                                </div> 
                                <div class="col-md-3 col-xs-6"> 
                                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                                </div> 
                                <div class="col-md-3 col-xs-6"> 
                                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                                </div> 
                            </div> 
                        </div>
                        <div class="caption"> 
                            <h3>Thumbnail label</h3> 
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
                            <p><a href="#" class="btn btn-primary" role="button">Button</a> 
                                <a href="#" class="btn btn-default" role="button">Button</a>
                            </p> 
                        </div> 
                    </div> 
                </div>
                <div class="col-sm-12">
                    <div class="thumbnail"> 
                        <div class="bs-example" data-example-id="simple-thumbnails"> 
                            <div class="row"> 
                                <div class="col-md-3 col-xs-6"> 
                                    <a href="#" class="thumbnail"><img alt="100%x180" data-src="/images/logo.jpg" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                                </div> 
                                <div class="col-md-3 col-xs-6"> 
                                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                                </div> 
                                <div class="col-md-3 col-xs-6"> 
                                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                                </div> 
                                <div class="col-md-3 col-xs-6"> 
                                    <a href="#" class="thumbnail"> <img alt="100%x180" data-src="holder.js/100%x180" style="height: 180px; width: 100%; display: block;" src="/images/logo.jpg" data-holder-rendered="true"> </a> 
                                </div> 
                            </div> 
                        </div>
                        <div class="caption"> 
                            <h3>Thumbnail label</h3> 
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p> 
                            <p><a href="#" class="btn btn-primary" role="button">Button</a> 
                                <a href="#" class="btn btn-default" role="button">Button</a>
                            </p> 
                        </div> 
                    </div> 
                </div>
                
            </div>
        </div>
        </div>
    </div>
    <div class="col-sm-12">
        <hr>
        <div class="container text-center">
            <h3>หมวดหมู่สินค้า / PRODUCT</h3>
        </div>
        <div class="col-sm-4">
            <a>ระบบห้องยา</a><br>
            <a>ระบบโรงจอดรถ</a><br>
            <a>ระบบเรียกคิว</a><br>
        </div>
        <div class="col-sm-4">
            <a>ป้าย LED</a><br>
            <a>ป้าย LED ขนาดใหญ่</a><br>
            <a>ไม้กั้นรถยนต์</a><br>
        </div>
        <div class="col-sm-4">
            <a>กล้องวงจรปิด</a><br>
            <a>ระบบคลัง</a><br>
            <a>ระบบห้องยา</a><br>
        </div>
    </div>
</div>