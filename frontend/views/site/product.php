<?php
use yii\widgets\Menu;
use yii\helpers\Html;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Nav;
use yii\bootstrap\Carousel;

$this->registerJs('$("#tab_C").addClass("active");');
$this->title = 'Andaman LED';

?>
<div class="container">
    <div class="col-sm-12">
        <div class="carousel-inner">
            <?php 
            echo Carousel::widget([
                'items' => [
                    [
                        'content' => '<img src="/images/test_2.jpg"/>',
                        'caption' => '<p>ติดตั้งระบบบัตรคิว และจอแสดงผลขนาดใหญ่โรงพยาบาลราชวิถี</p>',
                    ],
                        
                    [
                        'content' => '<img src="/images/test3.jpg"/>',
                        'caption' => '<p>ติดตั้งระบบบัตรคิว และจอแสดงผลขนาดใหญ่โรงพยาบาลราชวิถี</p>',
                    ],
                    [
                        'content' => '<img src="/images/test4.jpg"/>',
                        'caption' => '<p>ติดตั้งระบบบัตรคิว และจอแสดงผลขนาดใหญ่โรงพยาบาลราชวิถี</p>',
                    ],
                ],
                'controls' =>  [
                    '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>',
                    '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>'
                ],
                'options' => [
                    'class' => 'carousel slide'
                ]
            ]);?>
        </div><br>
    </div>
    <?php 
        if("id = tab_C_1"){
            echo $this->render('pms.php');
        }elseif ("id = tab_C_2") {
            echo $this->render('qms.php');
        }elseif ("id = tab_C_3") {
            echo $this->render('edb.php');
        } 
    ?>
</div>

    

<br>
